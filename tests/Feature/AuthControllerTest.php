<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    /**
     * @test
     */
    public function register()
    {
        $testEmail = 'test@test';
        User::where('email', $testEmail)->delete();
        $response = $this->post(
            route('register', [
                'email' => $testEmail,
                'name' => 'test',
                'password' => 'test',
            ])
        );
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas(User::class, [
            'email' => $testEmail
        ]);
    }

    /**
     * @test
     * @depends register
     */
    public function registerFailed()
    {
        //duplicate email
        $testEmail = 'test@test';
        $response = $this->postJson(
                route('register', [
                    'email' => $testEmail,
                    'name' => 'test',
                    'password' => 'test',
                    'password_confirmation' => 'test'
                ])
            );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        //wrong email format
        $response = $this->postJson(
                route('register', [
                    'email' => 'test',
                    'name' => 'test',
                    'password' => 'test',
                    'password_confirmation' => 'test'
                ])
            );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     * @depends register
     */
    public function login()
    {
        $response = $this->post(
            route('login', [
                'email' => 'test@test',
                'password' => 'test',
            ])
        );
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     * @depends register
     */
    public function loginFailed()
    {
        $response = $this->postJson(
            route('login', [
                'email' => 'test',
                'password' => '',
            ])
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     * @depends register
     */
    public function logout()
    {
        $response = $this->post(
            route('login', [
                'email' => 'test@test',
                'password' => 'test',
            ])
        );

        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $response['success']])->post(route('logout'));
        $response->assertStatus(Response::HTTP_OK);
    }
}
